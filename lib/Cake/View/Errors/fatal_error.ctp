<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>




	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __d('cake_dev', 'Fatal Error'); ?></h1>
			<h2><?php printf("<?php echo date('M'); ?>"); ?> <strong><?php printf("<?php echo date('d'); ?>"); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __d('cake_dev', 'Fatal Error'); ?></h3>
					<p class="error">
						<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
						<?php echo h($error->getMessage()); ?>
						<br>

						<strong><?php echo __d('cake_dev', 'File'); ?>: </strong>
						<?php echo h($error->getFile()); ?>
						<br>

						<strong><?php echo __d('cake_dev', 'Line'); ?>: </strong>
						<?php echo h($error->getLine()); ?>
					</p>
					<p class="notice">
						<strong><?php echo __d('cake_dev', 'Notice'); ?>: </strong>
						<?php echo __d('cake_dev', 'If you want to customize this error message, create %s', APP_DIR . DS . 'View' . DS . 'Errors' . DS . 'fatal_error.ctp'); ?>
					</p>
					
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->


