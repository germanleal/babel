<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>




	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __d('cake_dev', 'Missing View'); ?></h1>
			<h2><?php printf("<?php echo date('M'); ?>"); ?> <strong><?php printf("<?php echo date('d'); ?>"); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __d('cake_dev', 'Missing View'); ?></h3>
					<p class="error">
						<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
						<?php echo __d('cake_dev', 'The view for %1$s%2$s was not found.', '<em>' . h(Inflector::camelize($this->request->controller)) . 'Controller::</em>', '<em>' . h($this->request->action) . '()</em>'); ?>
					</p>
					<p class="error">
						<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
						<?php echo __d('cake_dev', 'Confirm you have created the file: %s', h($file)); ?>
					</p>

					<?php echo $this->element('exception_stack_trace'); ?>
					
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->
