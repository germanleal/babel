	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h1>
			<h2><?php printf("<?php echo date('M'); ?>"); ?> <strong><?php printf("<?php echo date('d'); ?>"); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h3>

					<table class="table responsive-table" id="sorting-example1">

						<tbody>
							<?php
								foreach ($fields as $field) {
									$isKey = false;
									if (!empty($associations['belongsTo'])) {
										foreach ($associations['belongsTo'] as $alias => $details) {
											if ($field === $details['foreignKey']) {
												$isKey = true;
												echo "\t<tr>\n";
												echo "\t\t<th scope=\"col\" class=\"align-center\"><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></th>\n";
												echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t&nbsp;\n\t\t</td>\n";
												echo "\t</tr>\n";
												break;
											}
										}
									}
									if($isKey !== true && $field!='created' && $field!='modified' && $field!='id' ){
										echo "\t<tr>\n";
										echo "\t\t<th scope=\"col\" class=\"align-center\"><?php echo __('" . Inflector::humanize($field) . "'); ?></th>\n";
										echo "\t\t<td>\n\t\t\t<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>\n\t\t\t&nbsp;\n\t\t</td>\n";
										echo "\t</tr>\n";
									}
								}
							?>
						</tbody>

					</table>
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->