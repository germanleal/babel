<script type="text/javascript" language="javascript">
	$(function(){
		
	});
	function envia_delete(id){
			if (confirm("Are you sure you want to delete # "+id)) { 
				$('.delete_'+id).submit(); 
			}
			return false;
		}
</script>	
	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h1>
			<h2><?php printf("<?php echo date('M'); ?>"); ?> <strong><?php printf("<?php echo date('d'); ?>"); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h3>

					<table class="table responsive-table" id="sorting-example1">

						<thead>
							<tr>
								<!--
								<th scope="col"><input type="checkbox" name="check-all" id="check-all" value="1"></th>
								<th scope="col">Text</th>
								<th scope="col" width="15%" class="align-center hide-on-mobile">Date</th>
								<th scope="col" width="15%" class="align-center hide-on-mobile-portrait">Status</th>
								<th scope="col" width="15%" class="hide-on-tablet">Tags</th>
								<th scope="col" width="100" class="align-right">Actions</th>
								-->
								<?php 
									$cantidad=0;
									foreach ($fields as $field){ 
										if($field!='created' && $field!='modified' && $field!='id' ){
								?>
									<th scope="col" class="align-center"><?php echo "<?php echo \$this->Paginator->sort('{$field}','".ucwords($field)."'); ?>"; ?></th>
								<?php 
											$cantidad++;
										}
									} 
								?>
								<th scope="col" width="100" class="align-center">Acciones</th>
								
							</tr>
						</thead>

						<tfoot>
							<tr>
								<td colspan="<?php echo ($cantidad+1); ?>">
									<?php echo "<?php
									echo \$this->Paginator->counter(array(
									'format' => __('P&aacute;gina {:page}/{:pages}, mostrando {:current} registro(s) de un total de {:count}')
									));
									?>"; ?>
									<div class="float-right">
										<style>
											.ddd,.ddd a{
												color:white;
											}
											.normala{
												color: #607890;
											}
											.normala a{
												color:white;
											}
										</style>
										<div class="button-group">
											<!--
											<a href="#" title="First page" class="button blue-gradient glossy"><span class="icon-previous"></span></a>
											<a href="#" title="Previous page" class="button blue-gradient glossy"><span class="icon-backward"></span></a>
											-->
											<?php
												$span_primero='<span class="icon-previous"></span>';
												$span_anterior='<span class="icon-backward"></span>';
												echo "<?php\n";
												// echo "\t\techo \$this->Paginator->first('".$span_primero."', array('class' => 'button blue-gradient glossy'), null, array('tag'=>false));\n";
												// echo "\t\techo \$this->Paginator->prev('".$span_anterior."', array('class' => 'button blue-gradient glossy'), null, array('tag'=>false));\n";
												
												
												echo "\t\techo \$this->Paginator->first('".$span_primero."', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));\n";
												echo "\t\techo \$this->Paginator->prev('".$span_anterior."', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));\n";
												echo "\t?>\n";
											?>
										</div>

										<div class="button-group">
											<!--
											<a href="#" title="Page 1" class="button blue-gradient glossy">1</a>
											<a href="#" title="Page 2" class="button blue-gradient glossy active">2</a>
											<a href="#" title="Page 3" class="button blue-gradient glossy">3</a>
											<a href="#" title="Page 4" class="button blue-gradient glossy">4</a>
											-->
											<?php
												echo "<?php\n";
												echo "\t\techo \$this->Paginator->numbers(array('class' => 'button blue-gradient glossy normala','separator'=>''));\n";
												echo "\t?>\n";
											?>
										</div>

										<div class="button-group">
											<!--
											<a href="#" title="Next page" class="button blue-gradient glossy"><span class="icon-forward"></span></a>
											<a href="#" title="Last page" class="button blue-gradient glossy"><span class="icon-next"></span></a>
											-->
											<?php
												$span_ultimo='<span class="icon-next"></span>';
												$span_siguiente='<span class="icon-forward"></span>';
												echo "<?php\n";
												echo "\t\techo \$this->Paginator->next('".$span_siguiente."', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));\n";
												echo "\t\techo \$this->Paginator->last('".$span_ultimo."', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));\n";
												echo "\t?>\n";
											?>
										</div>
									</div>
									
									
								</td>
							</tr>
						</tfoot>

						<tbody>
							<?php
								echo "<?php foreach (\${$pluralVar} as \${$singularVar}){ ?>\n";
								echo "\t<tr>\n";
									foreach ($fields as $field) {
										$isKey = false;
										if (!empty($associations['belongsTo'])) {
											foreach ($associations['belongsTo'] as $alias => $details) {
												if ($field === $details['foreignKey']) {
													$isKey = true;
													echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
													break;
												}
											}
										}
										if ($isKey !== true && $field!='created' && $field!='modified' && $field!='id' ) {
											
											echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
										}
									}
									echo "\t\t<td class=\"low-padding\">\n";
									echo "\t\t\t<span class=\"select compact full-width\" tabindex=\"0\">\n";
									echo "\t\t\t<?php echo \$this->Html->link('Ver', array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']),array('class'=>'select-value')); ?>\n";
									echo "\t\t\t\t<span class=\"select-arrow\"></span>\n";
									echo "\t\t\t\t<span class=\"drop-down\">\n";
									echo "\t\t\t<?php echo \$this->Html->link('Editar', array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
									echo "\t\t\t<?php echo \$this->Html->link('Borrar', array('url'=>'#'),array('onclick'=>'envia_delete(\"'.\${$singularVar}['{$modelClass}']['{$primaryKey}'].'\");return false;')); ?>\n";
									
									echo "\t\t\t\t</span>\n";
									echo "\t\t\t</span>\n";
									echo "\t\t</td>\n";
									
									
								echo "\t</tr>\n";
								echo "<?php echo \$this->Form->create('{$modelClass}',array('url' => array('action' => 'delete',\${$singularVar}['{$modelClass}']['{$primaryKey}']),'style'=>'display:none;','class'=>'delete_'.\${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
								
								echo "<?php echo \$this->Form->end(); ?>\n";
								echo "<?php } ?>\n";
							?>
						</tbody>

					</table>
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->