<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h1>
			<h2><?php printf("<?php echo date('M'); ?>"); ?> <strong><?php printf("<?php echo date('d'); ?>"); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h3>

					<fieldset class="fieldset fields-list">
						<?php echo "\t\t\t<?php echo \$this->Form->create('{$modelClass}',array('class'=>'FormObli')); ?>\n"; ?>
						<legend class="legend">Ingrese los campos <?php echo $action; ?></legend>
						<?php
							
							foreach ($fields as $field) {
								if (strpos($action, 'add') !== false && $field == $primaryKey) {
									continue;
								} elseif (!in_array($field, array('created', 'modified', 'updated','id'))) {
						?>
							<div class="field-block button-height">
								<label for="login" class="label"><b><?php echo ucwords(Inflector::humanize($field)); ?></b></label>
						<?php
								echo "\t\t\t\t<?php\n";
								echo "\t\t\t\t\techo \$this->Form->input('{$field}',array('class'=>'input full-width Obli','label'=>false,'div'=>false));\n";
								echo "\t\t\t\t?>\n";
						?>
							</div>
						<?php
								}else if (strpos($action, 'edit') !== false && $field == $primaryKey) {
								echo "\t\t\t\t<?php\n";
								echo "\t\t\t\t\techo \$this->Form->input('{$field}',array('class'=>'input full-width Obli','label'=>false,'div'=>false));\n";
								echo "\t\t\t\t?>\n";
								}
							}
							// if (!empty($associations['hasAndBelongsToMany'])) {
								// foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
									// echo "\t\techo \$this->Form->input('{$assocName}');\n";
								// }
							// }
							
						?>
						<div class="field-block button-height">

							<button type="submit" class="button glossy mid-margin-right">
								<span class="button-icon"><span class="icon-tick"></span></span>
								Guardar
							</button>

							<button type="submit" class="button glossy back-button">
								<span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
								Cancelar
							</button>

						</div>
						<?php
							echo "\t\t\t<?php echo \$this->Form->end(); ?>\n";
						?>
					</fieldset>

				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->
<script type="text/javascript" language="javascript">
	$(function(){
		$('.back-button').click(function(){
			history.back(); return false;
		});
		$('.FormObli').submit(function(){
			var enviar=1;
			$(".FormObli .Obli ").each( function() {			
				if ($.trim($(this).val()) ==''){
					//mostrar error
					$(this).css( "border-color", "#ff0039" );
					$(this).focus();
					// $.jGrowl('Debes ingresar un valor', { position: 'bottom-right',life: 8000 } );
					notify('Campo Obligatorio', 'Debes ingresar un valor', {
						system: true,
						vPos: 'bottom',
						showCloseOnHover: false
					});
					
					enviar=0;
					return false;
				}else{
					$(this).css( "border-color", "#bbbbbb" );
				}
			});
			
			if(enviar==0)return false;
			
		});
	});
</script>
