<?php
App::uses('AppController', 'Controller');
/**
 * MensajePredeterminados Controller
 *
 * @property MensajePredeterminado $MensajePredeterminado
 * @property PaginatorComponent $Paginator
 */
class MensajePredeterminadosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MensajePredeterminado->recursive = 0;
		$usuario=$this->Session->check('User');
		if($usuario['User']['grupo_id']==2){
			$conditions=array();
				
			$conditions[]=array(
				'OR'=>array(
					'MensajePredeterminado.edificio_id'=>$usuario['Departamento']['edificio_id'],
					'MensajePredeterminado.grupo_id'=>$usuario['User']['grupo_id'],
					array(
						'MensajePredeterminado.edificio_id IS NULL',
						'MensajePredeterminado.grupo_id IS NULL'
					)
				)
			);
			$this->paginate = array(        
				'conditions' => $conditions, 		
				'limit' => 10
			);
		}
		
		
		$this->set('mensajePredeterminados', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MensajePredeterminado->exists($id)) {
			throw new NotFoundException(__('Invalid mensaje predeterminado'));
		}
		$options = array('conditions' => array('MensajePredeterminado.' . $this->MensajePredeterminado->primaryKey => $id));
		$this->set('mensajePredeterminado', $this->MensajePredeterminado->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MensajePredeterminado->create();
			if ($this->MensajePredeterminado->save($this->request->data)) {
				$this->Session->setFlash(__('The mensaje predeterminado has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mensaje predeterminado could not be saved. Please, try again.'));
			}
		}
		$edificios = $this->MensajePredeterminado->Edificio->find('list');
		$grupos = $this->MensajePredeterminado->Grupo->find('list');
		$this->set(compact('edificios', 'grupos'));
	}
	
	public function crear() {
		
		if ($this->request->is('post')) {
			$this->MensajePredeterminado->create();
			if ($this->MensajePredeterminado->save($this->request->data)) {
				$this->Session->setFlash(__('Mensaje Guardado exitosamente.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mensaje predeterminado could not be saved. Please, try again.'));
			}
		}
		$edificios = $this->MensajePredeterminado->Edificio->find('list');
		$grupos = $this->MensajePredeterminado->Grupo->find('list');
		$this->set(compact('edificios', 'grupos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MensajePredeterminado->exists($id)) {
			throw new NotFoundException(__('Invalid mensaje predeterminado'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MensajePredeterminado->save($this->request->data)) {
				$this->Session->setFlash(__('The mensaje predeterminado has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mensaje predeterminado could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MensajePredeterminado.' . $this->MensajePredeterminado->primaryKey => $id));
			$this->request->data = $this->MensajePredeterminado->find('first', $options);
		}
		$edificios = $this->MensajePredeterminado->Edificio->find('list');
		$grupos = $this->MensajePredeterminado->Grupo->find('list');
		$this->set(compact('edificios', 'grupos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MensajePredeterminado->id = $id;
		if (!$this->MensajePredeterminado->exists()) {
			throw new NotFoundException(__('Invalid mensaje predeterminado'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MensajePredeterminado->delete()) {
			$this->Session->setFlash(__('The mensaje predeterminado has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mensaje predeterminado could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
