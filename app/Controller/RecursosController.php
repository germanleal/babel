<?php
App::uses('AppController', 'Controller');
/**
 * Recursos Controller
 *
 * @property Recurso $Recurso
 * @property PaginatorComponent $Paginator
 */
class RecursosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Recurso->recursive = 0;
		$this->set('recursos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Recurso->exists($id)) {
			throw new NotFoundException(__('Invalid recurso'));
		}
		$options = array('conditions' => array('Recurso.' . $this->Recurso->primaryKey => $id));
		$this->set('recurso', $this->Recurso->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Recurso->create();
			if ($this->Recurso->save($this->request->data)) {
				$this->Session->setFlash(__('The recurso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recurso could not be saved. Please, try again.'));
			}
		}
		$edificios = $this->Recurso->Edificio->find('list');
		$this->set(compact('edificios'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Recurso->exists($id)) {
			throw new NotFoundException(__('Invalid recurso'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Recurso->save($this->request->data)) {
				$this->Session->setFlash(__('The recurso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recurso could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Recurso.' . $this->Recurso->primaryKey => $id));
			$this->request->data = $this->Recurso->find('first', $options);
		}
		$edificios = $this->Recurso->Edificio->find('list');
		$tipoRecursos = $this->Recurso->TipoRecurso->find('list');
		$this->set(compact('edificios', 'tipoRecursos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Recurso->id = $id;
		if (!$this->Recurso->exists()) {
			throw new NotFoundException(__('Invalid recurso'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Recurso->delete()) {
			$this->Session->setFlash(__('The recurso has been deleted.'));
		} else {
			$this->Session->setFlash(__('The recurso could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
