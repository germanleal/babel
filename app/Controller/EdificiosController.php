<?php
App::uses('AppController', 'Controller');
/**
 * Edificios Controller
 *
 * @property Edificio $Edificio
 * @property PaginatorComponent $Paginator
 */
class EdificiosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Edificio->recursive = 0;
		$this->set('edificios', $this->Paginator->paginate());
		
		
	}

/**
 * Crear Depto method
 *
 * @return void
 */
	public function crear() {
		if ($this->request->is('post')) {
			$this->Edificio->create();
			if ($this->Edificio->save($this->request->data)) {
				$this->loadModel('Departamento');
				$this->loadModel('User');
				$edificio='e'.$this->Edificio->id.'d';
				if($this->request->data['Edificio']['numeration']==0){
					//letras
					$letras=array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
					for($i=0;$i<$this->request->data['Edificio']['numero_pisos'];$i++){
						for($j=1;$j<=$this->request->data['Edificio']['departamentos_por_piso'];$j++){
							$this->Departamento->create();
							if($j<10)$nombre=$letras[$i].'0'.$j;
							else $nombre=$letras[$i].$j;
							$departamento=$edificio.$nombre;
							$depto['Departamento']['nombre']=$nombre;
							$depto['Departamento']['edificio_id']=$this->Edificio->id;
							$depto['Departamento']['piso']=$i+1;
							$depto['Departamento']['n_piso']=$letras[$i];
							$this->Departamento->save($depto);
							
							$this->User->create();
							$user['User']['nombres']=$departamento;
							$user['User']['username']=$departamento;
							$user['User']['password']=md5($departamento);
							$user['User']['grupo_id']=4;
							$user['User']['departamento_id']=$this->Departamento->id;
							$this->User->save($user);
						}
					}
				}else{
					//numerica
					
					for($i=0;$i<$this->request->data['Edificio']['numero_pisos'];$i++){
						for($j=1;$j<=$this->request->data['Edificio']['departamentos_por_piso'];$j++){
							$this->Departamento->create();
							if($j<10)$nombre=($i+1).'0'.$j;
							else $nombre=($i+1).$j;
							$departamento=$edificio.$nombre;
							$depto['Departamento']['nombre']=$nombre;
							$depto['Departamento']['edificio_id']=$this->Edificio->id;
							$depto['Departamento']['piso']=$i+1;
							$depto['Departamento']['n_piso']=$i+1;
							$this->Departamento->save($depto);
							
							$this->User->create();
							$user['User']['nombres']=$departamento;
							$user['User']['username']=$departamento;
							$user['User']['password']=md5($departamento);
							$user['User']['grupo_id']=4;
							$user['User']['departamento_id']=$this->Departamento->id;
							$this->User->save($user);
						}
					}
				}
				
				
				$this->Session->setFlash(__('Edificio Creado Exitosamente.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The edificio could not be saved. Please, try again.'));
			}
		}
	}
	public function ver($id = null) {
		if (!$this->Edificio->exists($id)) {
			throw new NotFoundException(__('Invalid edificio'));
		}
		$options = array('conditions' => array('Edificio.' . $this->Edificio->primaryKey => $id));
		$this->set('edificio', $this->Edificio->find('first', $options));
		$this->loadModel('Departamento');
		$this->loadModel('User');
		
		$this->User->recursive = 0;
		
		$conditions=array();
			
		$conditions[]=array(
			'Departamento.edificio_id'=>$id
		);
		$this->paginate = array(        
			'conditions' => $conditions, 		
			'limit' => 10,
			'order' => array(
				'Departamento.created' => 'desc',
				'Departamento.nombre' => 'desc'
			)
		);
		$users=$this->paginate('User');
		
		$this->set(compact('users'));
	}
	public function agregar_departamento($edificio_id=null){
		$this->loadModel('Departamento');
		$this->loadModel('User');
		$edificio=$this->Edificio->findById($edificio_id);
		
		if ($this->request->is('post')) {
			$this->Departamento->create();
			$edificio='e'.$edificio_id.'d';
			$depto['Departamento']['nombre']=$this->request->data['Departamento']['nombre'];
			$depto['Departamento']['edificio_id']=$edificio_id;
			$depto['Departamento']['n_piso']=$this->request->data['Departamento']['n_piso'];
			$depto['Departamento']['piso']=$this->request->data['Departamento']['piso'];
			$this->Departamento->save($depto);
			
			$this->User->create();
			$user['User']['nombres']=$edificio.$this->request->data['Departamento']['nombre'];
			$user['User']['username']=$edificio.$this->request->data['Departamento']['nombre'];
			$user['User']['password']=md5($edificio.$this->request->data['Departamento']['nombre']);
			$user['User']['grupo_id']=4;
			$user['User']['departamento_id']=$this->Departamento->id;
			$this->User->save($user);
			
			$this->Session->setFlash(__('Departamento Creado Exitosamente.'));
			return $this->redirect(array('action' => 'ver',$edificio_id));
		}
		
		$this->set(compact('edificio'));
	}
	public function editar_departamento($departamento_id=null){
		$this->loadModel('Departamento');
		$this->loadModel('User');
		
		$departamento=$this->Departamento->findById($departamento_id);
		$edificio['Edificio']=$departamento['Edificio'];
		$usuario=$this->User->findByDepartamento_id($departamento_id);
		
		if ($this->request->is('post')) {
			// $this->Departamento->create();
			$edi='e'.$edificio['Edificio']['id'].'d';
			$depto['Departamento']['id']=$this->request->data['Departamento']['id'];
			$depto['Departamento']['nombre']=$this->request->data['Departamento']['nombre'];
			// $depto['Departamento']['edificio_id']=$edificio_id;
			$depto['Departamento']['n_piso']=$this->request->data['Departamento']['n_piso'];
			$depto['Departamento']['piso']=$this->request->data['Departamento']['piso'];
			$this->Departamento->save($depto);
			
			// $this->User->create();
			$user['User']['id']=$this->request->data['User']['id'];
			$user['User']['nombres']=$this->request->data['User']['nombres'];
			$user['User']['apellidos']=$this->request->data['User']['apellidos'];
			$user['User']['email']=$this->request->data['User']['email'];
			$user['User']['telefono']=$this->request->data['User']['telefono'];
			// $user['User']['username']=$edificio.$this->request->data['Departamento']['username'];
			// $user['User']['password']=md5($edificio.$this->request->data['Departamento']['nombre']);
			// $user['User']['grupo_id']=4;
			// $user['User']['departamento_id']=$this->Departamento->id;
			$this->User->save($user);
			
			$this->Session->setFlash(__('Departamento Editado Exitosamente.'));
			return $this->redirect(array('action' => 'ver',$edificio['Edificio']['id']));
		}
		
		$this->set(compact('edificio','departamento','usuario'));
	}
	public function ver_departamento($departamento_id=null){
		$this->loadModel('Departamento');
		$this->loadModel('User');
		
		$departamento=$this->Departamento->findById($departamento_id);
		$edificio['Edificio']=$departamento['Edificio'];
		$usuario=$this->User->findByDepartamento_id($departamento_id);
		
		
		
		$this->set(compact('edificio','departamento','usuario'));
	}
	public function borrar_departamento($departamento_id=null){
		$this->loadModel('Departamento');
		$this->loadModel('User');
		
		$departamento=$this->Departamento->findById($departamento_id);
		$edificio['Edificio']=$departamento['Edificio'];
		$usuario=$this->User->findByDepartamento_id($departamento_id);
		
		
		$this->Departamento->id = $departamento_id;
		if (!$this->Departamento->exists()) {
			throw new NotFoundException(__('Invalid edificio'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Edificio->delete()) {
			$this->Session->setFlash(__('Edificio Borrado exitosamente.'));
		} else {
			$this->Session->setFlash(__('The edificio could not be deleted. Please, try again.'));
		}
		$this->User->id = $usuario['User']['id'];
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid edificio'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('Usuario Borrado exitosamente.'));
		} else {
			$this->Session->setFlash(__('The edificio could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'ver',$edificio['Edificio']['id']));
	}
	
	
	public function enviar_mensaje($departamento_id=null){
		$this->loadModel('Departamento');
		$this->loadModel('User');
		$this->loadModel('MensajePredeterminado');
		$this->loadModel('Mensaje');
		
		$departamento=$this->Departamento->findById($departamento_id);
		$edificio['Edificio']=$departamento['Edificio'];
		$usuario=$this->User->findByDepartamento_id($departamento_id);
		
		$user=$this->Session->read('User');
		
		$mensajesp=$this->MensajePredeterminado->find('all',array(
			'conditions'=>array(
				'OR'=>array(
					'MensajePredeterminado.edificio_id'=>$user['Departamento']['edificio_id'],
					'MensajePredeterminado.grupo_id'=>$user['User']['grupo_id'],
					array(
						'MensajePredeterminado.edificio_id IS NULL',
						'MensajePredeterminado.grupo_id IS NULL'
					)
				)
			)
		));
		
		if ($this->request->is('post')) {
			$this->Mensaje->create();
			$d['Mensaje']['emisor_id']=$user['User']['id'];
			$d['Mensaje']['receptor_id']=$usuario['User']['grupo_id'];
			$d['Mensaje']['parent_id']=null;
			$d['Mensaje']['titulo']=$this->request->data['Mensaje']['titulo'];
			$d['Mensaje']['cuerpo']=$this->request->data['Mensaje']['cuerpo'];
			
			
			if ($this->Mensaje->save($d)) {
				$this->Session->setFlash(__('Mensaje enviado Exitosamente.'));
				return $this->redirect(array('controller'=>'mensajes','action' => 'enviados'));
			} else {
				$this->Session->setFlash(__('The mensaje could not be saved. Please, try again.'));
			}
		}
		
		
		$this->set(compact('edificio','departamento','usuario','mensajesp'));
		
	}
	
	public function carga_mensaje_predeterminado($id=null){
		$this->layout='ajax';
		$this->loadModel('MensajePredeterminado');
		$mp=$this->MensajePredeterminado->findById($id);
		$this->set(compact('mp'));
	}
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Edificio->exists($id)) {
			throw new NotFoundException(__('Invalid edificio'));
		}
		$options = array('conditions' => array('Edificio.' . $this->Edificio->primaryKey => $id));
		$this->set('edificio', $this->Edificio->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Edificio->create();
			if ($this->Edificio->save($this->request->data)) {
				$this->Session->setFlash(__('The edificio has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The edificio could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Edificio->exists($id)) {
			throw new NotFoundException(__('Invalid edificio'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Edificio->save($this->request->data)) {
				$this->Session->setFlash(__('The edificio has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The edificio could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Edificio.' . $this->Edificio->primaryKey => $id));
			$this->request->data = $this->Edificio->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Edificio->id = $id;
		if (!$this->Edificio->exists()) {
			throw new NotFoundException(__('Invalid edificio'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Edificio->delete()) {
			$this->Session->setFlash(__('The edificio has been deleted.'));
		} else {
			$this->Session->setFlash(__('The edificio could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
}
