<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}

	
	public function login(){
		$this->layout='login';
		
		
		if ($this->request->is('post')) {
			$this->loadModel('User');
			
			$username=$this->User->findByUsername($this->request->data['User']['username']);
			if($username!=null){
				$password=$this->User->find('first',array(
					'User.username'=>$this->request->data['User']['username'],
					'User.password'=>md5($this->request->data['User']['password'])
				));
				if($password!=null){
					$this->Session->write('User', $password);
					$this->Session->setFlash('Bienvenido!!');
					$this->redirect(array('action' => 'principal'));
				}else{
					$this->Session->setFlash('La password no coincide');	
				}
			}else{
				$this->Session->setFlash('Usuario no se encuentra');
			}
			
			// $this->Session->setFlash(__('The parameter could not be saved. Please, try again.'));
			
		}

	}
	public function logout(){
		$this->Session->delete('User');
		$this->Session->setFlash('Hasta Pronto!!');
		$this->redirect(array('action' => 'login'));
	}
	
	
	public function prueba(){
		$this->layout='default';
	}
	public function prueba_form(){
		$this->layout='default';
	}
	
	public function prueba_index(){
		$this->layout='default';
	}
}
