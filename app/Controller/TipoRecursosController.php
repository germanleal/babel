<?php
App::uses('AppController', 'Controller');
/**
 * TipoRecursos Controller
 *
 * @property TipoRecurso $TipoRecurso
 * @property PaginatorComponent $Paginator
 */
class TipoRecursosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TipoRecurso->recursive = 0;
		$this->set('tipoRecursos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TipoRecurso->exists($id)) {
			throw new NotFoundException(__('Invalid tipo recurso'));
		}
		$options = array('conditions' => array('TipoRecurso.' . $this->TipoRecurso->primaryKey => $id));
		$this->set('tipoRecurso', $this->TipoRecurso->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TipoRecurso->create();
			if ($this->TipoRecurso->save($this->request->data)) {
				$this->Session->setFlash(__('The tipo recurso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tipo recurso could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TipoRecurso->exists($id)) {
			throw new NotFoundException(__('Invalid tipo recurso'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TipoRecurso->save($this->request->data)) {
				$this->Session->setFlash(__('The tipo recurso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tipo recurso could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TipoRecurso.' . $this->TipoRecurso->primaryKey => $id));
			$this->request->data = $this->TipoRecurso->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TipoRecurso->id = $id;
		if (!$this->TipoRecurso->exists()) {
			throw new NotFoundException(__('Invalid tipo recurso'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TipoRecurso->delete()) {
			$this->Session->setFlash(__('The tipo recurso has been deleted.'));
		} else {
			$this->Session->setFlash(__('The tipo recurso could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
