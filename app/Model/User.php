<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Grupo $Grupo
 * @property Departamento $Departamento
 * @property Mensaje $MensajeEnviado
 * @property Mensaje $MensajeRecibido
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombres';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Grupo' => array(
			'className' => 'Grupo',
			'foreignKey' => 'grupo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Departamento' => array(
			'className' => 'Departamento',
			'foreignKey' => 'departamento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'MensajeEnviado' => array(
			'className' => 'Mensaje',
			'foreignKey' => 'emisor_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'MensajeRecibido' => array(
			'className' => 'Mensaje',
			'foreignKey' => 'receptor_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
