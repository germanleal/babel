<?php
App::uses('AppModel', 'Model');
/**
 * Mensaje Model
 *
 * @property Mensaje $ParentMensaje
 * @property User $Emisor
 * @property User $Receptor
 * @property Mensaje $ChildMensaje
 */
class Mensaje extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'titulo';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ParentMensaje' => array(
			'className' => 'Mensaje',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Emisor' => array(
			'className' => 'User',
			'foreignKey' => 'emisor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Receptor' => array(
			'className' => 'User',
			'foreignKey' => 'receptor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ChildMensaje' => array(
			'className' => 'Mensaje',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
