<?php
App::uses('AppModel', 'Model');
/**
 * Edificio Model
 *
 * @property Departamento $Departamento
 * @property MensajePredeterminado $MensajePredeterminado
 * @property Piso $Piso
 * @property Recurso $Recurso
 */
class Edificio extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Departamento' => array(
			'className' => 'Departamento',
			'foreignKey' => 'edificio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Aviso' => array(
			'className' => 'Aviso',
			'foreignKey' => 'edificio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'MensajePredeterminado' => array(
			'className' => 'MensajePredeterminado',
			'foreignKey' => 'edificio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		
		'Recurso' => array(
			'className' => 'Recurso',
			'foreignKey' => 'edificio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
