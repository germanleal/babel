<?php
App::uses('AppModel', 'Model');
/**
 * Departamento Model
 *
 * @property Edificio $Edificio
 * @property Piso $Piso
 * @property User $User
 */
class Vioaviso extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'aviso_id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Aviso' => array(
			'className' => 'Aviso',
			'foreignKey' => 'aviso_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


}
