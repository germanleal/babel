<?php
App::uses('AppModel', 'Model');
/**
 * Piso Model
 *
 * @property Edificio $Edificio
 * @property Departamento $Departamento
 */
class Piso extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'prefijo';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Edificio' => array(
			'className' => 'Edificio',
			'foreignKey' => 'edificio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Departamento' => array(
			'className' => 'Departamento',
			'foreignKey' => 'piso_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
