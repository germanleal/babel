<?php
App::uses('MensajePredeterminadosController', 'Controller');

/**
 * MensajePredeterminadosController Test Case
 *
 */
class MensajePredeterminadosControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mensaje_predeterminado',
		'app.edificio',
		'app.departamento',
		'app.piso',
		'app.user',
		'app.grupo'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
