<?php
App::uses('EdificiosController', 'Controller');

/**
 * EdificiosController Test Case
 *
 */
class EdificiosControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.edificio',
		'app.departamento',
		'app.piso',
		'app.user',
		'app.grupo',
		'app.mensaje_predeterminado',
		'app.mensaje'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
