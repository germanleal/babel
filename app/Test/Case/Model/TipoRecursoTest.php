<?php
App::uses('TipoRecurso', 'Model');

/**
 * TipoRecurso Test Case
 *
 */
class TipoRecursoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tipo_recurso',
		'app.recurso'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TipoRecurso = ClassRegistry::init('TipoRecurso');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TipoRecurso);

		parent::tearDown();
	}

}
