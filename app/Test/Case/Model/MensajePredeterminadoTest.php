<?php
App::uses('MensajePredeterminado', 'Model');

/**
 * MensajePredeterminado Test Case
 *
 */
class MensajePredeterminadoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mensaje_predeterminado',
		'app.edificio',
		'app.departamento',
		'app.piso',
		'app.user',
		'app.grupo'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MensajePredeterminado = ClassRegistry::init('MensajePredeterminado');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MensajePredeterminado);

		parent::tearDown();
	}

}
