<?php
/**
 * PisoFixture
 *
 */
class PisoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'numero_departamentos' => array('type' => 'integer', 'null' => true, 'default' => null),
		'prefijo' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 600, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'edificio_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'numero_departamentos' => 1,
			'prefijo' => 'Lorem ipsum dolor sit amet',
			'edificio_id' => 1,
			'created' => '2013-11-11 04:36:42',
			'modified' => '2013-11-11 04:36:42'
		),
	);

}
