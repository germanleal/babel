<?php
/**
 * DepartamentoFixture
 *
 */
class DepartamentoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 600, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'tipo_copropietario' => array('type' => 'integer', 'null' => true, 'default' => null),
		'edificio_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'piso_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'nombre' => 'Lorem ipsum dolor sit amet',
			'tipo_copropietario' => 1,
			'edificio_id' => 1,
			'piso_id' => 1,
			'created' => '2013-11-11 05:43:17',
			'modified' => '2013-11-11 05:43:17'
		),
	);

}
