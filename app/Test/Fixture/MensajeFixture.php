<?php
/**
 * MensajeFixture
 *
 */
class MensajeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'titulo' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 600, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'cuerpo' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'emisor_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'receptor_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'leido_emisor' => array('type' => 'integer', 'null' => true, 'default' => null),
		'borrado_emisor' => array('type' => 'integer', 'null' => true, 'default' => null),
		'leido_receptor' => array('type' => 'integer', 'null' => true, 'default' => null),
		'borrado_receptor' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'titulo' => 'Lorem ipsum dolor sit amet',
			'cuerpo' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'emisor_id' => 1,
			'receptor_id' => 1,
			'parent_id' => 1,
			'leido_emisor' => 1,
			'borrado_emisor' => 1,
			'leido_receptor' => 1,
			'borrado_receptor' => 1,
			'created' => '2013-11-11 04:42:08',
			'modified' => '2013-11-11 04:42:08'
		),
	);

}
