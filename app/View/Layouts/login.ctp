<!DOCTYPE html>

<!--[if IEMobile 7]><html class="no-js iem7 oldie linen"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie linen" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie linen" lang="en"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie9 linen" lang="en"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!--><html class="no-js linen" lang="en"><!--<![endif]-->
<head>
	<?php 
		echo $this->element('meta');
		echo $this->element('css_login');
		echo $this->element('js_login_up');
		echo $this->element('icons_login'); 
	?>
</head>

<body>
	<?php 
		echo $this->Session->flash();
		echo $this->fetch('content');
		// echo $this->element('sql_dump');
		echo $this->element('js_login_down'); 
	?>
</body>
</html>