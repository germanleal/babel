	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Tipo Recursos'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('View Tipo Recurso'); ?></h3>

					<table class="table responsive-table" id="sorting-example1">

						<tbody>
								<tr>
		<th scope="col" class="align-center"><?php echo __('Nombre'); ?></th>
		<td>
			<?php echo h($tipoRecurso['TipoRecurso']['nombre']); ?>
			&nbsp;
		</td>
	</tr>
						</tbody>

					</table>
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->