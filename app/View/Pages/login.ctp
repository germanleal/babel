	<div id="container">

		<hgroup id="login-title" class="large-margin-bottom">
			<h1 class="login-title-image">Babel</h1>
			<h5>&copy; Babel</h5>
		</hgroup>

		
		<?php
			echo $this->Form->create('User', array(
				'url' => array('controller' => 'pages', 'action' => 'login'),
				'id'=>'form-login'
			));
		?>
			<ul class="inputs black-input large">
				<!-- The autocomplete="off" attributes is the only way to prevent webkit browsers from filling the inputs with yellow -->
				<li>
					<span class="icon-user mid-margin-right"></span>
					
					<?php
						echo $this->Form->input('username',array('id'=>'login','class'=>'input-unstyled','label'=>false,'div'=>false,'placeholder'=>'Usuario'));
					?>
				</li>
				<li>
					<span class="icon-lock mid-margin-right"></span>
					<?php
						echo $this->Form->input('password',array('id'=>'pass','class'=>'input-unstyled','label'=>false,'div'=>false,'placeholder'=>'Password'));
					?>
				</li>
			</ul>

			<button type="submit" class="button glossy full-width huge">Entrar</button>
		<?php echo $this->Form->end(); ?>

	</div>