	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1>Dashboard</h1>
			<h2>nov <strong>10</strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline">Fields list</h3>

					<fieldset class="fieldset fields-list">

						<legend class="legend">Legend</legend>

						<div class="field-block button-height">
							<small class="input-info">This is the name that will be displayed on profile page</small>
							<label for="login" class="label"><b>User login</b></label>
							<input type="text" name="login" id="login" value="" class="input">
						</div>

						<div class="field-block button-height">
							<label for="file" class="label"><b>Avatar</b> (*.jpg)</label>
							<input type="file" name="file-input" id="file-input" value="" class="file">
							<small class="input-info">Max file size: 2MB</small>
						</div>

						<div class="field-drop button-height black-inputs">
							<label for="resize_height" class="label"><b>Resize height</b> (in px)</label>
							<span class="number input margin-right">
								<button type="button" class="button number-down">-</button>
								<input type="text" name="resize_height" id="resize_height" value="320" class="input-unstyled" data-number-options='{"min":100,"max":400}'>
								<button type="button" class="button number-up">+</button>
							</span>

							<input type="checkbox" name="crop" id="crop" class="switch medium" checked="checked"> &nbsp;
							<label for="crop">Enable crop</label>
						</div>

						<div class="field-block button-height">

							<button type="submit" class="button glossy mid-margin-right">
								<span class="button-icon"><span class="icon-tick"></span></span>
								Save
							</button>

							<button type="submit" class="button glossy">
								<span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
								Cancel
							</button>

						</div>

					</fieldset>

				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->