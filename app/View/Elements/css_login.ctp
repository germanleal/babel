	<!-- For all browsers -->
	<link rel="stylesheet" href="<?php echo $this->Html->url('/developer/'); ?>css/reset.css?v=1">
	<link rel="stylesheet" href="<?php echo $this->Html->url('/developer/'); ?>css/style.css?v=1">
	<link rel="stylesheet" href="<?php echo $this->Html->url('/developer/'); ?>css/colors.css?v=1">
	<link rel="stylesheet" media="print" href="<?php echo $this->Html->url('/developer/'); ?>css/print.css?v=1">
	<!-- For progressively larger displays -->
	<link rel="stylesheet" media="only all and (min-width: 480px)" href="<?php echo $this->Html->url('/developer/'); ?>css/480.css?v=1">
	<link rel="stylesheet" media="only all and (min-width: 768px)" href="<?php echo $this->Html->url('/developer/'); ?>css/768.css?v=1">
	<link rel="stylesheet" media="only all and (min-width: 992px)" href="<?php echo $this->Html->url('/developer/'); ?>css/992.css?v=1">
	<link rel="stylesheet" media="only all and (min-width: 1200px)" href="<?php echo $this->Html->url('/developer/'); ?>css/1200.css?v=1">
	<!-- For Retina displays -->
	<link rel="stylesheet" media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)" href="<?php echo $this->Html->url('/developer/'); ?>css/2x.css?v=1">

	<!-- Additional styles -->
	<link rel="stylesheet" href="<?php echo $this->Html->url('/developer/'); ?>css/styles/form.css?v=1">
	<link rel="stylesheet" href="<?php echo $this->Html->url('/developer/'); ?>css/styles/switches.css?v=1">

	<!-- Login pages styles -->
	<link rel="stylesheet" media="screen" href="<?php echo $this->Html->url('/developer/'); ?>css/login.css?v=1">