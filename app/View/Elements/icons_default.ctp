	<!-- For Modern Browsers -->
	<link rel="shortcut icon" href="<?php echo $this->Html->url('/developer/'); ?>img/favicons/favicon.png">
	<!-- For everything else -->
	<link rel="shortcut icon" href="<?php echo $this->Html->url('/developer/'); ?>img/favicons/favicon.ico">

	<!-- iOS web-app metas -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	
	<!-- iPhone ICON -->
	<link rel="apple-touch-icon" href="<?php echo $this->Html->url('/developer/'); ?>img/favicons/apple-touch-icon.png" sizes="57x57">
	<!-- iPad ICON -->
	<link rel="apple-touch-icon" href="<?php echo $this->Html->url('/developer/'); ?>img/favicons/apple-touch-icon-ipad.png" sizes="72x72">
	<!-- iPhone (Retina) ICON -->
	<link rel="apple-touch-icon" href="<?php echo $this->Html->url('/developer/'); ?>img/favicons/apple-touch-icon-retina.png" sizes="114x114">
	<!-- iPad (Retina) ICON -->
	<link rel="apple-touch-icon" href="<?php echo $this->Html->url('/developer/'); ?>img/favicons/apple-touch-icon-ipad-retina.png" sizes="144x144">

	<!-- iPhone SPLASHSCREEN (320x460) -->
	<link rel="apple-touch-startup-image" href="<?php echo $this->Html->url('/developer/'); ?>img/splash/iphone.png" media="(device-width: 320px)">
	<!-- iPhone (Retina) SPLASHSCREEN (640x960) -->
	<link rel="apple-touch-startup-image" href="<?php echo $this->Html->url('/developer/'); ?>img/splash/iphone-retina.png" media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)">
	<!-- iPhone 5 SPLASHSCREEN (640�1096) -->
	<link rel="apple-touch-startup-image" href="<?php echo $this->Html->url('/developer/'); ?>img/splash/iphone5.png" media="(device-height: 568px) and (-webkit-device-pixel-ratio: 2)">
	<!-- iPad (portrait) SPLASHSCREEN (748x1024) -->
	<link rel="apple-touch-startup-image" href="<?php echo $this->Html->url('/developer/'); ?>img/splash/ipad-portrait.png" media="(device-width: 768px) and (orientation: portrait)">
	<!-- iPad (landscape) SPLASHSCREEN (768x1004) -->
	<link rel="apple-touch-startup-image" href="<?php echo $this->Html->url('/developer/'); ?>img/splash/ipad-landscape.png" media="(device-width: 768px) and (orientation: landscape)">
	<!-- iPad (Retina, portrait) SPLASHSCREEN (2048x1496) -->
	<link rel="apple-touch-startup-image" href="<?php echo $this->Html->url('/developer/'); ?>img/splash/ipad-portrait-retina.png" media="(device-width: 1536px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2)">
	<!-- iPad (Retina, landscape) SPLASHSCREEN (1536x2008) -->
	<link rel="apple-touch-startup-image" href="<?php echo $this->Html->url('/developer/'); ?>img/splash/ipad-landscape-retina.png" media="(device-width: 1536px)  and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2)">

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
	
	<!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
	<meta name="application-name" content="Developr Admin Skin">
	<meta name="msapplication-tooltip" content="Cross-platform admin template.">
	<meta name="msapplication-starturl" content="<?php echo $this->Html->url('/'); ?>">
	<!-- These custom tasks are examples, you need to edit them to show actual pages -->
	<meta name="msapplication-task" content="name=Agenda;action-uri=<?php echo $this->Html->url('/'); ?>;icon-uri=<?php echo $this->Html->url('/developer/'); ?>img/favicons/favicon.ico">
	<meta name="msapplication-task" content="name=My profile;action-uri=<?php echo $this->Html->url('/',true); ?>profile.html;icon-uri=<?php echo $this->Html->url('/developer/',true); ?>img/favicons/favicon.ico">
	