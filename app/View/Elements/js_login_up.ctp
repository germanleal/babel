	<!-- JavaScript at bottom except for Modernizr -->
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/libs/modernizr.custom.js"></script>
	
	<!-- JavaScript at the bottom for fast page loading -->

	<!-- Scripts -->
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/libs/jquery-1.10.2.min.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/setup.js"></script>

	<!-- Template functions -->
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.input.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.message.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.notify.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.tooltip.js"></script>