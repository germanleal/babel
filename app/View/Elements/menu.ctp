	<!-- Sidebar/drop-down menu -->
	<section id="menu" role="complementary">

		<!-- This wrapper is used by several responsive layouts -->
		<div id="menu-content">

			<header>
				Administrator
			</header>

			<div id="profile">
				<img src="img/user.png" width="64" height="64" alt="User name" class="user-icon">
				Bienvenido!!
				<span class="name">
				<?php 
					if($this->Session->check('User')){
						$usuario=$this->Session->read('User');
						echo $usuario['User']['nombres'].' <b>'.$usuario['User']['apellidos'].'</b>';
					}else{
						echo ' <b>Invitado</b>';
					}
					
				?> 
				</span>
			</div>

			<!-- By default, this section is made for 4 icons, see the doc to learn how to change this, in "basic markup explained" -->
			<ul id="access" class="children-tooltip">
				<li><a href="inbox.html" title="Messages"><span class="icon-inbox"></span><span class="count">2</span></a></li>
				<li><a href="calendars.html" title="Calendar"><span class="icon-calendar"></span></a></li>
				<li><a href="login.html" title="Profile"><span class="icon-user"></span></a></li>
				<li><a href="<?php echo $this->Html->url('/pages/logout'); ?>" title="Salir"><span class="icon-gear"></span></a></li>
			</ul>

			<section class="navigable" data-navigable-options='{"backText":"Atras"}'>
				<ul class="big-menu">
					<?php
						$el_menu=array(
							'Administrador'=>array(
								'Submenu 1'=>array(
									'Submenu 1.1'=>array(
										'Submenu 1.1.1'=>'index22.html',
										'Submenu 1.1.2'=>'index22.html',
										'Submenu 1.1.3'=>'index22.html'
									),
									'Submenu 1.2'=>array(
										'Submenu 1.2.1'=>'index22.html',
										'Submenu 1.2.2'=>'index22.html',
										'Submenu 1.2.3'=>'index22.html'
									)
								),
								'prueba'=>$this->Html->url('/pages/prueba'),
								'otro2'=>'index.html',
								'otro3'=>'index.html'
							)
						);
						
						
						$models=array('User','Parameter','MensajePredeterminado','Mensaje','Grupo','Edificio','Departamento','TipoRecurso','Recurso');
						$controllers=array('users','parameters','mensaje_predeterminados','mensajes','grupos','edificios','departamentos','tipo_recursos','recursos');
						
						$menu=array();
						$i=0;
						foreach($models as $m){
							
							$menu['Administrador'][$m]['Todos']=$this->Html->url('/'.$controllers[$i].'/');
							if($m=='Edificio'){
								$menu['Administrador'][$m]['Agregar']=$this->Html->url('/'.$controllers[$i].'/crear');
							}else{
								$menu['Administrador'][$m]['Agregar']=$this->Html->url('/'.$controllers[$i].'/add');
							}
							
							$i++;
						}
						
						
						// $menu['Administrador']['Prueba']['Prueba']=$this->Html->url('/pages/prueba');
						// $menu['Administrador']['Prueba']['Prueba Form']=$this->Html->url('/pages/form');
						// $menu['Administrador']['Prueba']['Prueba Index']=$this->Html->url('/pages/todos');
						
						$menu['Administrador_Depto']['Mensajes Predeterminados']['Ver Todos']=$this->Html->url('/mensajepredeterminados/index');
						$menu['Administrador_Depto']['Mensajes Predeterminados']['Crear']=$this->Html->url('/mensajepredeterminados/crear');
						$menu['Administrador_Depto']['Mensajes']['Enviados']=$this->Html->url('/mensajes/enviados');
						$menu['Administrador_Depto']['Mensajes']['Recibidos']=$this->Html->url('/mensajes/recibidos');
						$menu['Administrador_Depto']['Departamentos']['Ver Departamentos']=$this->Html->url('/edificios/ver/'.$usuario['Departamento']['edificio_id']);
						$menu['Administrador_Depto']['Departamentos']['Crear Nuevo']=$this->Html->url('/edificios/agregar_departamento/'.$usuario['Departamento']['edificio_id']);
						
						
						foreach($menu as $key => $value){
							$cantidad=count($value);
							
								echo '<li class="with-right-arrow">';
									echo '<span><span class="list-count">'.$cantidad.'</span>'.$key.'</span>';
									echo '<ul class="big-menu">';
										foreach($value as $key2 => $value2){
											if(count($value2)==1){
												// debug($value2);
												echo '<li><a href="'.$value2.'">'.$key2.'</a></li>';
											}else{
												echo '<li class="with-right-arrow">';
													echo '<span><span class="list-count">'.count($value2).'</span>'.Inflector::pluralize($key2).'</span>';
													echo '<ul class="big-menu">';
													foreach($value2 as $key3 => $value3){
														if(Inflector::pluralize($key2)== $this->name && $this->params['action'] == 'index' && $key3 == 'Todos'){
															echo '<li><a href="'.$value3.'" class="current navigable-current">Ver Todos</a></li>';
														}else if(Inflector::pluralize($key2)== $this->name && $this->params['action'] == 'add' && $key3 == 'Agregar'){
															echo '<li><a href="'.$value3.'" class="current navigable-current">Crear</a></li>';
														}else{
															if($key3 == 'Todos'){
																echo '<li><a href="'.$value3.'">Ver Todos</a></li>';
															}else if($key3 == 'Agregar'){
																echo '<li><a href="'.$value3.'">Crear</a></li>';
															}else{
																echo '<li><a href="'.$value3.'">'.$key3.'</a></li>';
															}
															
														}
														
													}
													echo '</ul>';
												echo '</li>';
											}	
										}
										
									echo '</ul>';
								echo '</li>';
							
						}
					?>
					
				</ul>
			</section>
			
			
		</div>
		<!-- End content wrapper -->

	

	</section>
	<!-- End sidebar/drop-down menu -->