	<script src="<?php echo $this->Html->url('/developer/'); ?>js/setup.js"></script>

	<!-- Template functions -->
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.scroll.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.input.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.message.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.modal.js"></script>
	
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.notify.js"></script>
	
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.progress-slider.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.tooltip.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.confirm.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.agenda.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.tabs.js"></script>		<!-- Must be loaded last -->

	<!-- Tinycon -->
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/libs/tinycon.min.js"></script>
	
	<!-- glDatePicker -->
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>
	
	<!-- jQuery Form Validation -->
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>
	
	<!-- Plugins -->
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/libs/jquery.tablesorter.min.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/libs/DataTables/jquery.dataTables.min.js"></script>
	<script src="<?php echo $this->Html->url('/developer/'); ?>js/developr.navigable.js"></script>
	<script>
		// Call template init (optional, but faster if called manually)
		$.template.init();
		jQuery(document).ready(function(){
			if($("#flashMessage").is (':visible')){
				var htmlStr = $("#flashMessage").html();
				
				notify('', htmlStr, {
					system: true,
					vPos: 'bottom',
					showCloseOnHover: false
				});
				
				$("#flashMessage").hide();
			}
			
		});	
	</script>