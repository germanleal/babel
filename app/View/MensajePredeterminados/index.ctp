<script type="text/javascript" language="javascript">
	$(function(){
		
	});
	function envia_delete(id){
			if (confirm("Are you sure you want to delete # "+id)) { 
				$('.delete_'+id).submit(); 
			}
			return false;
		}
</script>	
	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Mensaje Predeterminados'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('Index Mensaje Predeterminado'); ?></h3>

					<table class="table responsive-table" id="sorting-example1">

						<thead>
							<tr>
								<!--
								<th scope="col"><input type="checkbox" name="check-all" id="check-all" value="1"></th>
								<th scope="col">Text</th>
								<th scope="col" width="15%" class="align-center hide-on-mobile">Date</th>
								<th scope="col" width="15%" class="align-center hide-on-mobile-portrait">Status</th>
								<th scope="col" width="15%" class="hide-on-tablet">Tags</th>
								<th scope="col" width="100" class="align-right">Actions</th>
								-->
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('titulo','Titulo'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('cuerpo','Cuerpo'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('edificio_id','Edificio_id'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('grupo_id','Grupo_id'); ?></th>
																<th scope="col" width="100" class="align-center">Acciones</th>
								
							</tr>
						</thead>

						<tfoot>
							<tr>
								<td colspan="5">
									<?php
									echo $this->Paginator->counter(array(
									'format' => __('P&aacute;gina {:page}/{:pages}, mostrando {:current} registro(s) de un total de {:count}')
									));
									?>									<div class="float-right">
										<style>
											.ddd,.ddd a{
												color:white;
											}
											.normala{
												color: #607890;
											}
											.normala a{
												color:white;
											}
										</style>
										<div class="button-group">
											<!--
											<a href="#" title="First page" class="button blue-gradient glossy"><span class="icon-previous"></span></a>
											<a href="#" title="Previous page" class="button blue-gradient glossy"><span class="icon-backward"></span></a>
											-->
											<?php
		echo $this->Paginator->first('<span class="icon-previous"></span>', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));
		echo $this->Paginator->prev('<span class="icon-backward"></span>', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));
	?>
										</div>

										<div class="button-group">
											<!--
											<a href="#" title="Page 1" class="button blue-gradient glossy">1</a>
											<a href="#" title="Page 2" class="button blue-gradient glossy active">2</a>
											<a href="#" title="Page 3" class="button blue-gradient glossy">3</a>
											<a href="#" title="Page 4" class="button blue-gradient glossy">4</a>
											-->
											<?php
		echo $this->Paginator->numbers(array('class' => 'button blue-gradient glossy normala','separator'=>''));
	?>
										</div>

										<div class="button-group">
											<!--
											<a href="#" title="Next page" class="button blue-gradient glossy"><span class="icon-forward"></span></a>
											<a href="#" title="Last page" class="button blue-gradient glossy"><span class="icon-next"></span></a>
											-->
											<?php
		echo $this->Paginator->next('<span class="icon-forward"></span>', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));
		echo $this->Paginator->last('<span class="icon-next"></span>', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));
	?>
										</div>
									</div>
									
									
								</td>
							</tr>
						</tfoot>

						<tbody>
							<?php foreach ($mensajePredeterminados as $mensajePredeterminado){ ?>
	<tr>
		<td><?php echo h($mensajePredeterminado['MensajePredeterminado']['titulo']); ?>&nbsp;</td>
		<td><?php echo h($mensajePredeterminado['MensajePredeterminado']['cuerpo']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($mensajePredeterminado['Edificio']['nombre'], array('controller' => 'edificios', 'action' => 'view', $mensajePredeterminado['Edificio']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($mensajePredeterminado['Grupo']['nombre'], array('controller' => 'grupos', 'action' => 'view', $mensajePredeterminado['Grupo']['id'])); ?>
		</td>
		<td class="low-padding">
			<span class="select compact full-width" tabindex="0">
			<?php echo $this->Html->link('Ver', array('action' => 'view', $mensajePredeterminado['MensajePredeterminado']['id']),array('class'=>'select-value')); ?>
				<span class="select-arrow"></span>
				<span class="drop-down">
			<?php echo $this->Html->link('Editar', array('action' => 'edit', $mensajePredeterminado['MensajePredeterminado']['id'])); ?>
			<?php echo $this->Html->link('Borrar', array('url'=>'#'),array('onclick'=>'envia_delete("'.$mensajePredeterminado['MensajePredeterminado']['id'].'");return false;')); ?>
				</span>
			</span>
		</td>
	</tr>
<?php echo $this->Form->create('MensajePredeterminado',array('url' => array('action' => 'delete',$mensajePredeterminado['MensajePredeterminado']['id']),'style'=>'display:none;','class'=>'delete_'.$mensajePredeterminado['MensajePredeterminado']['id'])); ?>
<?php echo $this->Form->end(); ?>
<?php } ?>
						</tbody>

					</table>
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->