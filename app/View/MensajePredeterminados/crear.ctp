<?php
	$usuario=$this->Session->read('User');
?>
	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Mensajes Predeterminados'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('Crear Mensajes Predeterminado'); ?></h3>

					<fieldset class="fieldset fields-list">
					<?php echo $this->Form->create('MensajePredeterminado',array('class'=>'FormObli','url' => array('controller' => 'mensajepredeterminados', 'action' => 'crear'))); ?>
						<legend class="legend">Ingrese los campos requeridos</legend>
						<div class="field-block button-height">
								<label for="login" class="label"><b>Titulo Mensaje</b></label>
										<?php
					echo $this->Form->input('titulo',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
							<div class="field-block button-height">
								<label for="login" class="label"><b>Cuerpo Mensaje</b></label>
										<?php
					echo $this->Form->input('cuerpo',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
							
							<?php
								if($usuario['User']['grupo_id']==1){
							?>
							<div class="field-block button-height">
								<label for="login" class="label"><b>Edificio</b></label>
								<?php
									echo $this->Form->input('edificio_id',array('class'=>'input full-width','empty' => 'Todos','label'=>false,'div'=>false));
								?>
							</div>
							
							
							<div class="field-block button-height">
								<label for="login" class="label"><b>Grupo de Usuario</b></label>
								<?php
									echo $this->Form->input('grupo_id',array('class'=>'input full-width','empty' => 'Todos','label'=>false,'div'=>false));
								?>
							</div>
							<?php
								}else{
									echo $this->Form->input('edificio_id',array('type'=>'hidden','value'=>$usuario['Departamento']['edificio_id']));
							?>
							
							
							<br />
							<br />
							<br />
							<div class="field-block button-height">
								<label for="login" class="label"><b>Grupo de Usuario</b></label>
								<?php
									$opciones=array(
										'1'=>'Administradores',
										'2'=>'Conserjes',
										'3'=>'Copropietarios'
									);
									echo $this->Form->input('grupo_id',array('class'=>'input full-width','empty' => 'Todos','options'=>$opciones,'label'=>false,'div'=>false));
								?>
							</div>
							
							<?php
								}
							?>
							
							<div class="field-block button-height">

							<button type="submit" class="button glossy mid-margin-right">
								<span class="button-icon"><span class="icon-tick"></span></span>
								Guardar
							</button>

							<button type="submit" class="button glossy back-button">
								<span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
								Cancelar
							</button>

						</div>
									<?php echo $this->Form->end(); ?>
					</fieldset>

				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->
<script type="text/javascript" language="javascript">
	$(function(){
		$('.back-button').click(function(){
			history.back(); return false;
		});
		$('.FormObli').submit(function(){
			var enviar=1;
			$(".FormObli .Obli ").each( function() {			
				if ($.trim($(this).val()) ==''){
					//mostrar error
					$(this).css( "border-color", "#ff0039" );
					$(this).focus();
					// $.jGrowl('Debes ingresar un valor', { position: 'bottom-right',life: 8000 } );
					notify('Campo Obligatorio', 'Debes ingresar un valor', {
						system: true,
						vPos: 'bottom',
						showCloseOnHover: false
					});
					
					enviar=0;
					return false;
				}else{
					$(this).css( "border-color", "#bbbbbb" );
				}
			});
			
			if(enviar==0)return false;
			
		});
	});
</script>
