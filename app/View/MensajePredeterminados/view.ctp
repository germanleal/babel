	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Mensaje Predeterminados'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('View Mensaje Predeterminado'); ?></h3>

					<table class="table responsive-table" id="sorting-example1">

						<tbody>
								<tr>
		<th scope="col" class="align-center"><?php echo __('Titulo'); ?></th>
		<td>
			<?php echo h($mensajePredeterminado['MensajePredeterminado']['titulo']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th scope="col" class="align-center"><?php echo __('Cuerpo'); ?></th>
		<td>
			<?php echo h($mensajePredeterminado['MensajePredeterminado']['cuerpo']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th scope="col" class="align-center"><?php echo __('Edificio'); ?></th>
		<td>
			<?php echo $this->Html->link($mensajePredeterminado['Edificio']['nombre'], array('controller' => 'edificios', 'action' => 'view', $mensajePredeterminado['Edificio']['id'])); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th scope="col" class="align-center"><?php echo __('Grupo'); ?></th>
		<td>
			<?php echo $this->Html->link($mensajePredeterminado['Grupo']['nombre'], array('controller' => 'grupos', 'action' => 'view', $mensajePredeterminado['Grupo']['id'])); ?>
			&nbsp;
		</td>
	</tr>
						</tbody>

					</table>
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->