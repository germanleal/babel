	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Mensajes'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('Ver Mensaje'); ?></h3>

					<table class="table responsive-table" id="sorting-example1">

						<tbody>
								<tr>
		<th scope="col" class="align-center"><?php echo __('Titulo'); ?></th>
		<td>
			<?php echo h($mensaje['Mensaje']['titulo']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th scope="col" class="align-center"><?php echo __('Cuerpo'); ?></th>
		<td>
			<?php echo h($mensaje['Mensaje']['cuerpo']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th scope="col" class="align-center"><?php echo __('Emisor'); ?></th>
		<td>
			<?php echo $this->Html->link($mensaje['Emisor']['nombres'], array('controller' => 'users', 'action' => 'view', $mensaje['Emisor']['id'])); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th scope="col" class="align-center"><?php echo __('Receptor'); ?></th>
		<td>
			<?php echo $this->Html->link($mensaje['Receptor']['nombres'], array('controller' => 'users', 'action' => 'view', $mensaje['Receptor']['id'])); ?>
			&nbsp;
		</td>
	</tr>
	
						</tbody>

					</table>
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->