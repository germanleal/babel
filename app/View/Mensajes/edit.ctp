	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Mensajes'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('Edit Mensaje'); ?></h3>

					<fieldset class="fieldset fields-list">
									<?php echo $this->Form->create('Mensaje',array('class'=>'FormObli')); ?>
						<legend class="legend">Ingrese los campos edit</legend>
										<?php
					echo $this->Form->input('id',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							<div class="field-block button-height">
								<label for="login" class="label"><b>Titulo</b></label>
										<?php
					echo $this->Form->input('titulo',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
													<div class="field-block button-height">
								<label for="login" class="label"><b>Cuerpo</b></label>
										<?php
					echo $this->Form->input('cuerpo',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
													<div class="field-block button-height">
								<label for="login" class="label"><b>Emisor Id</b></label>
										<?php
					echo $this->Form->input('emisor_id',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
													<div class="field-block button-height">
								<label for="login" class="label"><b>Receptor Id</b></label>
										<?php
					echo $this->Form->input('receptor_id',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
													<div class="field-block button-height">
								<label for="login" class="label"><b>Parent Id</b></label>
										<?php
					echo $this->Form->input('parent_id',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
													<div class="field-block button-height">
								<label for="login" class="label"><b>Leido Emisor</b></label>
										<?php
					echo $this->Form->input('leido_emisor',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
													<div class="field-block button-height">
								<label for="login" class="label"><b>Borrado Emisor</b></label>
										<?php
					echo $this->Form->input('borrado_emisor',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
													<div class="field-block button-height">
								<label for="login" class="label"><b>Leido Receptor</b></label>
										<?php
					echo $this->Form->input('leido_receptor',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
													<div class="field-block button-height">
								<label for="login" class="label"><b>Borrado Receptor</b></label>
										<?php
					echo $this->Form->input('borrado_receptor',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
												<div class="field-block button-height">

							<button type="submit" class="button glossy mid-margin-right">
								<span class="button-icon"><span class="icon-tick"></span></span>
								Guardar
							</button>

							<button type="submit" class="button glossy back-button">
								<span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
								Cancelar
							</button>

						</div>
									<?php echo $this->Form->end(); ?>
					</fieldset>

				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->
<script type="text/javascript" language="javascript">
	$(function(){
		$('.back-button').click(function(){
			history.back(); return false;
		});
		$('.FormObli').submit(function(){
			var enviar=1;
			$(".FormObli .Obli ").each( function() {			
				if ($.trim($(this).val()) ==''){
					//mostrar error
					$(this).css( "border-color", "#ff0039" );
					$(this).focus();
					// $.jGrowl('Debes ingresar un valor', { position: 'bottom-right',life: 8000 } );
					notify('Campo Obligatorio', 'Debes ingresar un valor', {
						system: true,
						vPos: 'bottom',
						showCloseOnHover: false
					});
					
					enviar=0;
					return false;
				}else{
					$(this).css( "border-color", "#bbbbbb" );
				}
			});
			
			if(enviar==0)return false;
			
		});
	});
</script>
