<script type="text/javascript" language="javascript">
	$(function(){
		
	});
	function envia_delete(id){
			if (confirm("Are you sure you want to delete # "+id)) { 
				$('.delete_'+id).submit(); 
			}
			return false;
		}
</script>	
	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Mensajes'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('Index Mensaje'); ?></h3>

					<table class="table responsive-table" id="sorting-example1">

						<thead>
							<tr>
								<!--
								<th scope="col"><input type="checkbox" name="check-all" id="check-all" value="1"></th>
								<th scope="col">Text</th>
								<th scope="col" width="15%" class="align-center hide-on-mobile">Date</th>
								<th scope="col" width="15%" class="align-center hide-on-mobile-portrait">Status</th>
								<th scope="col" width="15%" class="hide-on-tablet">Tags</th>
								<th scope="col" width="100" class="align-right">Actions</th>
								-->
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('titulo','Titulo'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('cuerpo','Cuerpo'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('emisor_id','Emisor_id'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('receptor_id','Receptor_id'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('parent_id','Parent_id'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('leido_emisor','Leido_emisor'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('borrado_emisor','Borrado_emisor'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('leido_receptor','Leido_receptor'); ?></th>
																	<th scope="col" class="align-center"><?php echo $this->Paginator->sort('borrado_receptor','Borrado_receptor'); ?></th>
																<th scope="col" width="100" class="align-center">Acciones</th>
								
							</tr>
						</thead>

						<tfoot>
							<tr>
								<td colspan="10">
									<?php
									echo $this->Paginator->counter(array(
									'format' => __('P&aacute;gina {:page}/{:pages}, mostrando {:current} registro(s) de un total de {:count}')
									));
									?>									<div class="float-right">
										<style>
											.ddd,.ddd a{
												color:white;
											}
											.normala{
												color: #607890;
											}
											.normala a{
												color:white;
											}
										</style>
										<div class="button-group">
											<!--
											<a href="#" title="First page" class="button blue-gradient glossy"><span class="icon-previous"></span></a>
											<a href="#" title="Previous page" class="button blue-gradient glossy"><span class="icon-backward"></span></a>
											-->
											<?php
		echo $this->Paginator->first('<span class="icon-previous"></span>', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));
		echo $this->Paginator->prev('<span class="icon-backward"></span>', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));
	?>
										</div>

										<div class="button-group">
											<!--
											<a href="#" title="Page 1" class="button blue-gradient glossy">1</a>
											<a href="#" title="Page 2" class="button blue-gradient glossy active">2</a>
											<a href="#" title="Page 3" class="button blue-gradient glossy">3</a>
											<a href="#" title="Page 4" class="button blue-gradient glossy">4</a>
											-->
											<?php
		echo $this->Paginator->numbers(array('class' => 'button blue-gradient glossy normala','separator'=>''));
	?>
										</div>

										<div class="button-group">
											<!--
											<a href="#" title="Next page" class="button blue-gradient glossy"><span class="icon-forward"></span></a>
											<a href="#" title="Last page" class="button blue-gradient glossy"><span class="icon-next"></span></a>
											-->
											<?php
		echo $this->Paginator->next('<span class="icon-forward"></span>', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));
		echo $this->Paginator->last('<span class="icon-next"></span>', array('escape'=>false,'class'=>'button blue-gradient glossy ddd'));
	?>
										</div>
									</div>
									
									
								</td>
							</tr>
						</tfoot>

						<tbody>
							<?php foreach ($mensajes as $mensaje){ ?>
	<tr>
		<td><?php echo h($mensaje['Mensaje']['titulo']); ?>&nbsp;</td>
		<td><?php echo h($mensaje['Mensaje']['cuerpo']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($mensaje['Emisor']['nombres'], array('controller' => 'users', 'action' => 'view', $mensaje['Emisor']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($mensaje['Receptor']['nombres'], array('controller' => 'users', 'action' => 'view', $mensaje['Receptor']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($mensaje['ParentMensaje']['titulo'], array('controller' => 'mensajes', 'action' => 'view', $mensaje['ParentMensaje']['id'])); ?>
		</td>
		<td><?php echo h($mensaje['Mensaje']['leido_emisor']); ?>&nbsp;</td>
		<td><?php echo h($mensaje['Mensaje']['borrado_emisor']); ?>&nbsp;</td>
		<td><?php echo h($mensaje['Mensaje']['leido_receptor']); ?>&nbsp;</td>
		<td><?php echo h($mensaje['Mensaje']['borrado_receptor']); ?>&nbsp;</td>
		<td class="low-padding">
			<span class="select compact full-width" tabindex="0">
			<?php echo $this->Html->link('Ver', array('action' => 'view', $mensaje['Mensaje']['id']),array('class'=>'select-value')); ?>
				<span class="select-arrow"></span>
				<span class="drop-down">
			<?php echo $this->Html->link('Editar', array('action' => 'edit', $mensaje['Mensaje']['id'])); ?>
			<?php echo $this->Html->link('Borrar', array('url'=>'#'),array('onclick'=>'envia_delete("'.$mensaje['Mensaje']['id'].'");return false;')); ?>
				</span>
			</span>
		</td>
	</tr>
<?php echo $this->Form->create('Mensaje',array('url' => array('action' => 'delete',$mensaje['Mensaje']['id']),'style'=>'display:none;','class'=>'delete_'.$mensaje['Mensaje']['id'])); ?>
<?php echo $this->Form->end(); ?>
<?php } ?>
						</tbody>

					</table>
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->