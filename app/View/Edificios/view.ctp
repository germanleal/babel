	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Edificios'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('View Edificio'); ?></h3>

					<table class="table responsive-table" id="sorting-example1">

						<tbody>
								<tr>
		<th scope="col" class="align-center"><?php echo __('Nombre'); ?></th>
		<td>
			<?php echo h($edificio['Edificio']['nombre']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th scope="col" class="align-center"><?php echo __('Logo'); ?></th>
		<td>
			<?php echo h($edificio['Edificio']['logo']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th scope="col" class="align-center"><?php echo __('Descripcion'); ?></th>
		<td>
			<?php echo h($edificio['Edificio']['descripcion']); ?>
			&nbsp;
		</td>
	</tr>
	<tr>
		<th scope="col" class="align-center"><?php echo __('Numero Pisos'); ?></th>
		<td>
			<?php echo h($edificio['Edificio']['numero_pisos']); ?>
			&nbsp;
		</td>
	</tr>
						</tbody>

					</table>
				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->