<?php
	// $usuario=$this->Session->read('User');
?>
	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Enviar Mensaje'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('Enviar Mensaje'); ?></h3>

					<fieldset class="fieldset fields-list">
					<?php echo $this->Form->create('MensajePredeterminado',array('class'=>'FormObli','url' => array('controller' => 'edificios', 'action' => 'enviar_mensaje',$departamento['Departamento']['id']))); ?>
						<legend class="legend">Ingrese los campos requeridos</legend>
							<div class="field-block button-height">
								<label for="login" class="label"><b>Para:</b></label>
								<?php
									echo $this->Form->input('para',array('class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>'Departamento '.$departamento['Departamento']['nombre']));
								?>
								<?php
									echo $this->Form->input('para',array('class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>$usuario['User']['nombres'].' '.$usuario['User']['apellidos']));
								?>
							</div>
							<div class="field-block button-height">
								<label for="login" class="label"><b>Titulo Mensaje</b></label>
										<?php
					echo $this->Form->input('Mensaje.titulo',array('class'=>'input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
							
							<div class="field-block button-height">
								<label for="login" class="label"><b>Mensaje Predeterminado</b></label>
								<?php
									$opciones=array();
									foreach($mensajesp as $mm){
										$opciones[$mm['MensajePredeterminado']['id']]=$mm['MensajePredeterminado']['titulo'];
									}
									echo $this->Form->input('mensaje_p_id',array('class'=>'mp input full-width','empty' => 'Ninguno','options'=>$opciones,'label'=>false,'div'=>false));
								?>
							</div>
							<br />
							<br />
							<br />
							<div class="field-block button-height">
								<label for="login" class="label"><b>Cuerpo Mensaje</b></label>
										<?php
					echo $this->Form->input('Mensaje.cuerpo',array('class'=>'cuerpo input full-width Obli','label'=>false,'div'=>false));
				?>
							</div>
							
							
							
							<div class="field-block button-height">

							<button type="submit" class="button glossy mid-margin-right">
								<span class="button-icon"><span class="icon-tick"></span></span>
								Guardar
							</button>

							<button type="submit" class="button glossy back-button">
								<span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
								Cancelar
							</button>

						</div>
									<?php echo $this->Form->end(); ?>
					</fieldset>

				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->
<script type="text/javascript" language="javascript">
	$(function(){
		$('.back-button').click(function(){
			history.back(); return false;
		});
		$('.mp').change(function() {
			
			$('.cuerpo').load('<?php echo $this->Html->url('/edificios/carga_mensaje_predeterminado/'); ?>'+$(this).val());
		});
		$('.FormObli').submit(function(){
			var enviar=1;
			$(".FormObli .Obli ").each( function() {			
				if ($.trim($(this).val()) ==''){
					//mostrar error
					$(this).css( "border-color", "#ff0039" );
					$(this).focus();
					// $.jGrowl('Debes ingresar un valor', { position: 'bottom-right',life: 8000 } );
					notify('Campo Obligatorio', 'Debes ingresar un valor', {
						system: true,
						vPos: 'bottom',
						showCloseOnHover: false
					});
					
					enviar=0;
					return false;
				}else{
					$(this).css( "border-color", "#bbbbbb" );
				}
			});
			
			if(enviar==0)return false;
			
		});
	});
</script>
