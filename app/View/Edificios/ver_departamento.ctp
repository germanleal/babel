	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h1><?php echo __('Departamentos'); ?></h1>
			<h2><?php echo date('M'); ?> <strong><?php echo date('d'); ?></strong></h2>
		</hgroup>

		

		<div class="with-padding">

			<div class="columns">

				<div class="twelve-columns twelve-columns-tablet">

					<h3 class="thin underline"><?php echo __('Ver Datos Departamento'); ?></h3>

					<fieldset class="fieldset fields-list">
						<?php echo $this->Form->create('Departamento',array('class'=>'FormObli','url' => array('controller' => 'edificios', 'action' => 'ver_departamento',$departamento['Departamento']['id']))); ?>
						<legend class="legend">Datos Departamento</legend>
							<div class="field-block button-height">
								<label for="login" class="label"><b>Nombre Departamento</b></label>
								<?php
									echo $this->Form->input('Departamento.id',array('type'=>'hidden','value'=>$departamento['Departamento']['id']));
									echo $this->Form->input('Departamento.nombre',array('readonly'=>'readonly','class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>$departamento['Departamento']['nombre']));
									
								?>
							</div>
							<div class="field-block button-height">
								<label for="login" class="label"><b>Nombre Piso</b></label>
								<?php
									echo $this->Form->input('Departamento.n_piso',array('readonly'=>'readonly','class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>$departamento['Departamento']['n_piso']));
								?>
							</div>
							<div class="field-block button-height">
								<label for="login" class="label"><b>Nombre Piso</b></label>
								<?php
									echo $this->Form->input('Departamento.piso',array('readonly'=>'readonly','class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>$departamento['Departamento']['piso']));
								?>
							</div>
					</fieldset>
					<fieldset class="fieldset fields-list">
						<legend class="legend">Datos cuenta Copropietario</legend>
							<div class="field-block button-height">
								<label for="login" class="label"><b>Usuario</b></label>
								<?php
									echo $this->Form->input('User.id',array('type'=>'hidden','value'=>$usuario['User']['id']));
									echo $this->Form->input('User.username',array('readonly'=>'readonly','class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>$usuario['User']['username']));
								?>
							</div>					
							<div class="field-block button-height">
								<label for="login" class="label"><b>Nombres</b></label>
								<?php
									echo $this->Form->input('User.nombres',array('readonly'=>'readonly','class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>$usuario['User']['nombres']));
								?>
							</div>	
							<div class="field-block button-height">
								<label for="login" class="label"><b>Apellidos</b></label>
								<?php
									echo $this->Form->input('User.apellidos',array('readonly'=>'readonly','class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>$usuario['User']['apellidos']));
								?>
							</div>	
							
							<div class="field-block button-height">
								<label for="login" class="label"><b>Email</b></label>
								<?php
									echo $this->Form->input('User.email',array('readonly'=>'readonly','class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>$usuario['User']['email']));
								?>
							</div>	
							<div class="field-block button-height">
								<label for="login" class="label"><b>Telefono</b></label>
								<?php
									echo $this->Form->input('User.telefono',array('readonly'=>'readonly','class'=>'input full-width Obli','label'=>false,'div'=>false,'value'=>$usuario['User']['telefono']));
								?>
							</div>	

							
						
									<?php echo $this->Form->end(); ?>
					</fieldset>

				</div>
			</div>

		</div>

	</section>
	<!-- End main content -->
<script type="text/javascript" language="javascript">
	$(function(){
		$('.back-button').click(function(){
			history.back(); return false;
		});
		$('.FormObli').submit(function(){
			var enviar=1;
			$(".FormObli .Obli ").each( function() {			
				if ($.trim($(this).val()) ==''){
					//mostrar error
					$(this).css( "border-color", "#ff0039" );
					$(this).focus();
					// $.jGrowl('Debes ingresar un valor', { position: 'bottom-right',life: 8000 } );
					notify('Campo Obligatorio', 'Debes ingresar un valor', {
						system: true,
						vPos: 'bottom',
						showCloseOnHover: false
					});
					
					enviar=0;
					return false;
				}else{
					$(this).css( "border-color", "#bbbbbb" );
				}
			});
			
			if(enviar==0)return false;
			
		});
	});
</script>
